;********************************************************
;关于保护模式的代码： GDT LDT 分段 分页 特权级 中断		*
;进入保护模式的主要步骤:								*
;	准备GDT												*
;	用lgdt加载gdtr										*
;	打开A2O地址线										*
;	设置cr0寄存器										*
;	跳转到保护模式代码									*
;********************************************************

%include "pm_info.inc"

	org		07c00h

	jmp	text_start

[SECTION .gdt]
;GDT
GDT_DESC_ZERO:			Descriptor		0,			0,				0		;空描述符，第一个描述符总是为空	
GDT_DESC_VIDEO:			Descriptor		0B8000H,	0FFFFH,			DA_DRW	;80*25 模式显存
GDT_DESC_VIDEO_VGA:		Descriptor		0A0000H,	0FFFFH,			DA_DRW	;VGA模式显存
GDT_DESC_CODE32:		Descriptor		0,			Code32Len-1,	DA_E + 	DA_32 ;32位代码段
;GDT end

;要使用GDT 必须要得到GDT的长度和界限，并填写到GdtPtr这个48个字节的结构中
GdtLen		equ		$ - GDT_DESC_ZERO	;GDT 长度
GdtPtr		DW		GdtLen - 1			;GDT 界限 地址从0开始 所以要减1
			DD		0					;GDT 基地址

;要使用GDT还必须有GDT选择子 GDT选择子暂时可以理解为当前描述符到GDT基地址的偏移
Selector_gdt_code32		equ		GDT_DESC_CODE32 - GDT_DESC_ZERO
Selector_gdt_video		equ		GDT_DESC_VIDEO - GDT_DESC_ZERO
Selector_gdt_video_vga	equ		GDT_DESC_VIDEO_VGA - GDT_DESC_ZERO
;[SECTION .gdt] end




[SECTION .t16]
[BITS	16]
text_start:

	;调用BIOS 10H号中断 设置显卡
	;mov	AL, 013H
	;mov	AH, 0
	;INT	010H


	mov	ax, cs			;cs段由CPU赋值的段值
	mov	ds, ax
	mov	es,	ax
	mov	ss, ax
	mov	sp, 0100h		;指定偏移0100h为栈底


	;必须要初始化32位代码段的基地址
	xor	eax, eax
	mov	ax, cs
	shl	eax, 4
	add eax, code32_start
	mov	word [GDT_DESC_CODE32 + 2], ax
	shr	eax, 16
	mov	byte [GDT_DESC_CODE32 + 4], al
	mov	byte [GDT_DESC_CODE32 + 7], ah

	;GDT 必须要加载到gdt专用寄存器中才能使用
	xor	eax, eax
	mov	ax, ds
	shl eax, 4
	add eax, GDT_DESC_ZERO			;eax为GDT的基地址
	mov dword [GdtPtr + 2], eax		;放入结构中
	
	;加载GDTR
	lgdt	[GdtPtr]

	;关中断
	cli			;某些操作需要原子的进行不能被打断所以要关闭CPU对中断的响应

	;打开A20地址线 如果不打开 将不能访问全部内存
	in al, 92h	;把92H端口号的数据读进al
	or al, 00000010b		;把第二位置1
	out 92h, al


	;准备切换到保护模式		cr0寄存器的第0位(PE位) 置1可进入保护模式
	mov eax, cr0
	or	eax, 1
	mov	cr0, eax

	jmp	dword Selector_gdt_code32:0		;执行这句会把Selector_gdt_code32装入cs 
										;并跳转到Selector_gdt_code32:0处
										;Selector_gdt_code32是GDT选择子




[SECTION .t32]
[BITS	32]
code32_start:
	;填充屏幕
;	mov	ax, Selector_gdt_video_vga
;	mov	gs, ax
;
;	mov	ecx, 0FFFFH
;	mov	edi, 0
;
;	mov	ah, 0
;	mov	al,	15H
;.loop:
;	mov	ax, di
;	and	ax, 0FH
;	mov	[gs:edi], ax
;	cmp ecx, 0
;	inc	edi
;	dec	ecx
;	jne .loop
;

	mov	ax, Selector_gdt_video
	mov	gs, ax
	mov	ah,	0CH
	mov	al,	'w'
	mov	edi, (80 * 17 + 20) *2 
	mov	[gs:edi], ax

	hlt

Code32Len	equ		$ - code32_start

