;****************************************************
;	TIME:	2015-09-17								*
;	AUTHOR:	HL_WANG									*
;	NOTE:											*
;		boot.asm 编写512字节的引导扇区				*
;		从软盘引导，是因为还没有实现磁盘文件系统，	*
;		当完成磁盘文件系统以后，实现从磁盘引导		*
;		编译方式: nasm boot.asm -o boo.bin			*
;		用DD命令写入软盘镜像:						*
;	dd if=xxx of=xx1 bs=512 count=1, conv=notrunc	*
;	xdd -a xxx.img 查看二进制文件信息，				*
;	-a参数用*代替值为0的输出						*
;****************************************************

	org		0x7c00							;告诉编译器程序要装载到内存0x7c00处执行。

	;FAT12引导扇区的头，占62个字节
	jmp		LABEL_START						;跳过启动扇区数据
	nop										;空一个字节，偏移为0，占3个字节

	BS_OEMName			DB		'os_c    '	;厂商名字，偏移为3，占8个字节
	BPB_BytsPerSec		DW		0x200		;每扇区字节数，偏移为11，占2个字节
	BPB_SecPerClus		DB		0x01		;每簇扇区数，偏移为13，占1个字节
	BPB_RsvdSecCnt		DW		0x01		;Boot记录占用扇区数，偏移为14，占2个字节
	BPB_NumFATs			DB		0x02		;共有多少FAT表，偏移为16，占1个字节
	BPB_RootEnCnt		DW		0xE0		;根目录文件数最大值，偏移为17，占2个字节
	BPB_TolSec16		DW		0xB40		;扇区总数，偏移为19，占2个字节
	BPB_Media			DB		0xF0		;介质描述符，偏移为21，占1个字节
	BPB_FATSz16			DW		0x09		;每FAT扇区数，偏移为22，占2个字节
	BPB_SecPerTrk		DW		0x12		;每磁道扇区数，偏移为24，占2个字节
	BPB_NumHeads		DW		0x2			;磁头数，偏移为26，占2个字节
	BPB_HiddSec			DD		0x0			;隐藏扇区数，偏移为28，占4个字节
	BPB_TotSec32		DD		0xB40		;如果BPB_TotSec16为0，由这个值记录扇区数，偏移为32，占4个字节
	BS_DrvNum			DB		0x0			;中断13h的驱动器号，偏移为36，占1个字节
	BS_Reserved1		DB		0x0			;未使用，偏移为37，占1个字节
	BS_BootSig			DB		0x29		;扩展引导标记，偏移为38，占1个字节
	BS_VoID				DD		0x0			;卷序列号，偏移为39，占4个字节
	BS_VolLab			DB		'os_c_VolLab';卷标，偏移为43，占11个字节
	BS_FileSysType		DB		'FAT12   '	;文件系统类型，偏移为54，占8个字节


;Offset 62 偏移62开始 到结束标志之间可以存入引导代码，数据和填充字符
LABEL_START:
	;在这里可以添加代码,把Loader加载入内存，然后跳转到Loader的位置开始执行Loader,
	;一旦跳转完成，Boot的使命就结束了。


	;测试
	;显示一个字符串，调用BIOS的10H中断。
	;初始化寄存器
	mov	ax, cs		;代码段
	mov	ds, ax		;数据段
	mov	es,	ax		;数据段
	call	Dispay	;

;显示一行字符
Dispay:
	mov	ax, message
	mov	bp, ax			;ES:BP  字符串地址
	mov	cx, 16			;字符串长度
	mov	ax, 01301h		;ah = 13 在Teletype模式下显示字符串, al=01 文字40*25 16位颜色 
	mov	bx, 000ch		;颜色
	mov	dl, 0			;起始列
	int 10h				;BIOS 10h号中断


	message		db		'Hello2, bios int !',0

	times	510 -($-$$)		db		0			;偏移 62 -- 510 全部填0 

;offset 510
END						DW		0xAA55		;结束标志
